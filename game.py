from random import randint
name = input("Hi! What is your name?")

for guess_number in range(5):
    month_number = randint (1,12)#need to generate a random number
    year_number = randint (1924, 2004)#need to generate a random number
    print("Guess " + str(guess_number + 1) + ":" + name + " were you born on " + str(month_number) + "/" + str(year_number) + "?")
    answer = input("yes or no?")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no":
        if guess_number == 4:
            print("I have other things to do. Goodbye")
        else:
            print("Drat! Lemme try again!")


# print("Guess 5: " + name + " were you born on " + str(month_number) + "/" + str(year_number) + "?")
# answer = input("yes or no?")
# if answer == "yes":
#     print("I knew it!")
#     exit()
# elif answer == "no":
#     print("I have other things to do. Goodbye")
